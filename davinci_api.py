import openai

# Initialize the API key
openai.api_key = ""

# Define the prompt
root_prompt = "Give one subject area for a niche blog. Only give the name, no additional info"

# Generate the response
response = openai.Completion.create(
    engine="text-davinci-003",
    prompt=root_prompt,
    max_tokens=1024,
    n=1,
    stop=None,
    temperature=0.5,
)

# Store the niche subject area in a variable
niche_subject = response.choices[0].text.strip()

# Define the prompt to get topic ideas based on the niche subject area
topic_prompt = f"Generate 5 topic ideas for a {niche_subject} blog, just give the topic ideas, nothing more, each on their own line, make them short, do not number or bullet the lines, provide no extra formatting"
print(niche_subject + "\n")
# Generate the response for the topic ideas
response = openai.Completion.create(
    engine="text-davinci-003",
    prompt=topic_prompt,
    max_tokens=1024,
    n=1,
    stop=None,
    temperature=0.5,
)

# Store each topic idea in a separate variable
topic_ideas = response.choices[0].text.strip().split("\n")
for idea in topic_ideas:
    print(idea + "\n")
# Define the prompt to generate a post based on a topic idea
post_prompt = "Generate a short blog post based on the following topic idea:\n"

# Loop through the topic ideas and generate a post for each one
for i, topic_idea in enumerate(topic_ideas):
    print(f"Generating post {i+1}/{len(topic_ideas)}")
    post_response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=post_prompt + topic_idea,
        max_tokens=1024,
        n=1,
        stop=None,
        temperature=0.5,
    )
    # Store the post in a variable or save it to a file
    post_text = post_response.choices[0].text.strip()
    print(post_text)